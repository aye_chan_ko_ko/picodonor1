import React from 'react';

function Show(){
    alert("Message.");
}
export default function SignIn(){
    return(
        <div class="main-body">
            <div class="header">
                <h2>
                    <a href="HomePage.html"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" 
                    onclick=" var mainBody=document.getElementById('main-body').style.visibility='hidden'"><path d="M9 1C4.58 1 1 4.58 1 9s3.58 8 8 8 8-3.58 8-8-3.58-8-8-8zm4 10.87L11.87 13 9 10.13 6.13 13 5 11.87 7.87 9 5 6.13 6.13 5 9 7.87 11.87 5 13 6.13 10.13 9 13 11.87z"/></svg></a>								
                    Sign In</h2>
            </div>
            <div class="text-body">
                <h1>Hello,stranger</h1>
                <h1 id="next-text">Please tell us who you are<br/> before continuing</h1>
            </div>
        
            <div class="md-form">
                <input placeholder="Name" type="text" id="inputName"/>
                <br/><br/>
                <input placeholder="Email address" type="text" id="inputEmail"/>
            </div>
            <div class="done" onclick={Show}><h3>Done</h3></div>
        </div>
    );
}