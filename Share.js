import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";
export default function Share(){
        
    let history = useHistory();
    console.log(history)
    let back = e => {
       e.stopPropagation();
       history.goBack();
       };
   return(
   // <div className="container">
   //     <div class="w3-bar w3-green">
   //     <p class="w3-bar-item w3-left">Share app</p>
   //     <a href="HomePage.html"><p class="w3-bar-item w3-right">Done</p></a>
   //     </div>
   //     <div class="container">
   //     <br/><br/>
   //     <p><img src="share.jpg" alt="share" width="200px" height="200px"></p><br/>
   //     <h3 style="text-align: center;">Donor List</h3>
   //     <p>Share this app by scanning the code with your phone's camera.</p>
   //     <p><input type="button" value="SHARE LINK" style="text-align: center;color: green;"/></p>
   //     </div>
   // </div>
   <div>
       <div className="w3-bar w3-green">
       <p className="w3-bar-item w3-left">Share app</p>
       {/* <a href="HomePage.html"><p className="w3-bar-item w3-right">Done</p></a> */}
       <button className="w3-bar-item w3-right" onClick={back}>Done</button>
       </div>
       <br/><br/>
       <div className="container">
       <p style={{textAlign:'center'}}><img src={require('../src/share.jpg')} alt="share" width="200px" height="200px"/></p><br/>
       <h3 style={{textAlign:'center'}}>Donor List</h3>
       <p style={{textAlign:'center'}}>Share this app by scanning the code with your phone's camera.</p>
       <p style={{textAlign:'center'}}><input type="button" value="SHARE LINK" style={{textAlign:'center',color:'green'}}/></p>
       </div>
   </div>
       
   
   );
}