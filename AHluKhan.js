import React from 'react'
import './homePageStyle.css'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";

class AHluKhan extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            display:"none",
            search:'',
            searchOpen:"block",
            searchClose:"none",
            DonerName:[
                {id:1,name:"University of Mandalay"},{id:2,name:"Dagon University Yangon"},
                {id:3,name:"Mandalay University"},{id:4,name:"Technological University Mandalay"},
                {id:5,name:"Yadanarbon University"},{id:6,name:"KyaukSe University"},
                {id:7,name:"Sa Gaing University"},{id:8,name:"Madical University"},
                {id:9,name:"ThanLwin University"},{id:10,name:"Traditional Madical University"},
                {id:11,name:"Myanguan University"},{id:12,name:"PaThein University"},
                {id:13,name:"MyitKyiNa University"},{id:14,name:"Computer University"}
            ]
        }
    }
    w3open= ()=> {
        this.setState({
            display:"block"
        })
      }
    w3close=()=>{
        this.setState({
            display:'none'
        })
    }
    w3SearchOpen=()=>{
        this.setState({
            searchOpen:"none",
            searchClose:"block"
        })
    }
    w3SearchClose=()=>
    {
        this.setState({
            searchClose:"none",
            searchOpen:"block"
        })
    }
    HandleSearch=e=>
    {
        this.setState({search:e.target.value});
    }

render()
{
    let filterName=this.state.DonerName.filter(
        (Names)=>{
            return Names.name.toLowerCase().indexOf(this.state.search.toLowerCase())!==-1;
        }
    );
    return(
        <div style={{height:100+'%'}}>
            <div className="w3-sidebar w3-bar-block w3-border-right" style={{display:this.state.display,id:'mySidebar',zIndex:2}}>
            {/* <button onClick={(e)=>this.w3close(e)} className="w3-bar-item w3-large">Close &times;</button> */}
            <a href="SignIn.html" className="w3-bar-item w3-button w3-green" style={{height:80+'px'}}><i className="fa fa-user-circle" style={{width:30+'px',height:30+'px'}}></i><br/>Sign In</a>
            <Link to="/item" className="w3-bar-item w3-button">Items</Link>
            <Link to="/ppe" className="w3-bar-item w3-button">PPE</Link>
            <Link to="cctv" className="w3-bar-item w3-button">CCTV</Link>
            <Link to="/sesinthu" className="w3-bar-item w3-button">စီစင်သူ</Link>
            {/* <a href="SharePage.html" className="w3-bar-item w3-button">Share app</a>             */}
            <Link to="/sharePage" className="w3-bar-item w3-button">Share App</Link>
            </div>
            <nav className="navbar navbar-expand-sm w3-green fixed-top" style={{zIndex:1}}>
            <div className="w3-bar w3-green">
                <button className="w3-bar-item w3-button w3-green w3-xlarge w3-left" id="btn1" onClick={(e)=>this.w3open(e)}>☰</button>
                <input type="text" placeholder="Search.." onChange={(e)=>this.HandleSearch(e)} className="w3-bar-item" style={{borderRadius:20,width:50+'%',marginTop:7,marginLeft:10+'%',display:this.state.searchClose}} id="searchBtn1"/>
                <button className="w3-bar-item" style={{display:this.state.searchClose,marginTop: 12,backgroundColor:'gray'}} id="searchBtn2" onClick={(e)=>this.w3SearchClose(e)}><i className="fa fa-times"></i></button>
                <Link to="./registrationForm"><button className="w3-bar-item w3-button w3-green w3-xlarge w3-right" id="searchBtn4" style={{display: 'block'}}><i class="fa fa-plus"></i></button></Link>
                <button className="w3-bar-item w3-button w3-green w3-xlarge w3-right" id="searchBtn3" onClick={(e)=>this.w3SearchOpen(e)} style={{display:this.state.searchOpen}}><i className="fa fa-search"></i></button>
                <div className="w3-container" onClick={()=>this.w3close()}>
                <h1>အလှူခံ</h1>
                </div>
            </div>
            </nav>

            <div className="w3-container" onClick={()=>this.w3close()}>
                <table className="w3-table w3-bordered" id="changeTable1">
                <tbody>
                    {filterName.map((v,key)=>
                        {
                            let index=v.name.indexOf(' ');
                            return(
                                <tr key={v.id}>
                                    <td>
                                    <Link to={{pathname: `/ahlukhan/${v.id}`}}>
                                    
                                    {/* <p className="w3-left">{v.name}<br/>{v.value}<br/>{v.price}</p><img src={v.imgName} className="w3-right" id="imgStyle" alt="picture"></img> */}
                            <p className="w3-left">{v.name}</p><i className="fas fa-angle-right w3-right"> </i>                                    
                                    </Link>
                                    </td>
                            
                                </tr>
                            );
                        })
                    }
                    <tr style={{height:50}}></tr>
                </tbody>
                </table>
            </div>
            <div className="footer">
            <table id="changeTable2">
            <tfoot onClick={()=>this.w3close()}>
            <tr>
            <td><Link to="./ahluShin"><i className="fas fa-donate" style={{color:'black'}}></i><br/><p>အလှူရှင်</p></Link></td>
            <td><Link to="./ahlukhan"><i className="fa fa-medkit" style={{color:'black'}}></i><br/><p>အလှူခံ</p></Link></td>
            <td><Link to="./sayinmyar"><i className="fas fa-newspaper" style={{color:'black'}}></i><br/><p>စာရင်းများ</p></Link></td>
            <td><a href="#"><i className="fas fa-comment" style={{color:'black'}}></i><br/><p>Chat</p></a></td>
            </tr>
            </tfoot>
            
            </table>
            </div>
        </div>    
    );
}
}
export default AHluKhan

