import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";
  const DonerName=[
    {id:1,name:"University of Mandalay"},{id:2,name:"Dagon University Yangon"},
    {id:3,name:"Mandalay University"},{id:4,name:"Technological University Mandalay"},
    {id:5,name:"Yadanarbon University"},{id:6,name:"KyaukSe University"},
    {id:7,name:"Sa Gaing University"},{id:8,name:"Madical University"},
    {id:9,name:"ThanLwin University"},{id:10,name:"Traditional Madical University"},
    {id:11,name:"Myanguan University"},{id:12,name:"PaThein University"},
    {id:13,name:"MyitKyiNa University"},{id:14,name:"Computer University"}
]
export default function InnerAHluKhan() {
    let history = useHistory();
    console.log(history)
    let back = e => {
       e.stopPropagation();
       history.goBack();
       };
      let { id } = useParams();
      let image = DonerName[parseInt(id-1, 10)];
    // console.log(id)
      if (!image) return <div>Image not found</div>;
    
      return (
        <div>
          <nav className="navbar navbar-expand-sm w3-green fixed-top" style={{width: 100+'%',height: 50+'px'}}>
            <div>
            <i className='fas fa-arrow-left' style={{size: 20+'px',fontSize: 20+'px'}} onClick={back}>အလှူရှင်တည်နေရာ</i>
            </div>            
          </nav>
            <div className="container" style={{width:100+'%',position:'absolute',top:60}}>
             <h3>အလှူရှင် တည်နေရာ</h3>
             <p>{image.name}</p>
            <small>Qty Needed</small>
            <p>8</p>
            <small>Qty Donate</small>
            <p>7</p>
            <small>City/Township</small>
            <p>Town</p>
            <div className="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d118427.04428123902!2d96.00578293871503!3d21.94050429858575!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cb6d23f0d27411%3A0x24146be01e4e5646!2sMandalay!5e0!3m2!1sen!2smm!4v1586260130052!5m2!1sen!2smm"
            //  width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
            style={{width:100+'%',height:250}}></iframe>
            </div>
            </div>
            {/* <div className="footer">
              <table id="changeTable2">
                  <tr style={{color:"black"}}> 
                  <td><Link to="./ahluShin"><i className="fas fa-donate" style={{color:'black'}}></i><br/><p>အလှူရှင်</p></Link></td>
                <td><Link to="./ahlukhan"><i className="fa fa-medkit" style={{color:'black'}}></i><br/><p>အလှူခံ</p></Link></td>
                      <td><i class="fas fa-newspaper" style={{color:'black'}}></i><br/>စာရင်းများ</td>
                      <td><i class="fas fa-comment" style={{color:'black'}}></i><br/>Chat</td>
                  </tr>
              </table>
            </div> */}
        </div>
      );
    }
  