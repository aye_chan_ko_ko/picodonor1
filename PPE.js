import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";

  const DonerName=[
    {id:1,name:"University of Mandalay"},{id:2,name:"Dagon University Yangon"},
    {id:3,name:"Mandalay University"},{id:4,name:"Technological University Mandalay"},
    {id:5,name:"Yadanarbon University"},{id:6,name:"KyaukSe University"},
    {id:7,name:"Sa Gaing University"},{id:8,name:"Madical University"},
    {id:9,name:"ThanLwin University"},{id:10,name:"Traditional Madical University"},
    {id:11,name:"Myanguan University"},{id:12,name:"PaThein University"},
    {id:13,name:"MyitKyiNa University"},{id:14,name:"Computer University"}
]
export default function PPE() {
  let history = useHistory();
  console.log(history)
  let back = e => {
     e.stopPropagation();
     history.goBack();
     };
      return (
        <div>
          <nav className="navbar navbar-expand-sm w3-green fixed-top" style={{width: 100+'%',height: 50+'px'}}>
            <div>
            <i className='fas fa-arrow-left' onClick={back} style={{size: 20+'px',fontSize: 20+'px'}}>PPE</i>
            </div>            
          </nav>
            <div className="w3-container">
                <table className="w3-table w3-bordered" style={{position:'absolute',top:50,left:0}}>
                <tbody>
                    {DonerName.map((v,key)=>
                        {
                            let index=v.name.indexOf(' ');
                            return(
                                <tr key={v.id}>
                                    <td>
                                    <Link to={{pathname: `/ahlukhan/${v.id}`}}>
                                    
                                    {/* <p className="w3-left">{v.name}<br/>{v.value}<br/>{v.price}</p><img src={v.imgName} className="w3-right" id="imgStyle" alt="picture"></img> */}
                            <p className="w3-left">{v.name}</p><i className="fas fa-angle-right w3-right"> </i>                                    
                                    </Link>
                                    </td>
                            
                                </tr>
                            );
                        })
                    }
                </tbody>
                </table>
            </div>
            {/* <div className="footer">
              <table id="changeTable2">
                  <tr style={{color:"black"}}> 
                  <td><Link to="./ahluShin"><i className="fas fa-donate" style={{color:'black'}}></i><br/><p>အလှူရှင်</p></Link></td>
                <td><Link to="./ahlukhan"><i className="fa fa-medkit" style={{color:'black'}}></i><br/><p>အလှူခံ</p></Link></td>
                      <td><i class="fas fa-newspaper" style={{color:'black'}}></i><br/>စာရင်းများ</td>
                      <td><i class="fas fa-comment" style={{color:'black'}}></i><br/>Chat</td>
                  </tr>
              </table>
            </div> */}
        </div>
      );
    }
  