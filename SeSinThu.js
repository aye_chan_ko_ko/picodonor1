import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";
import './homePageStyle.css'

export default function SeSinThu(){
    let history = useHistory();
    console.log(history)
    let back = e => {
       e.stopPropagation();
       history.goBack();
       };
    return(
<div>
    <nav class="navbar navbar-expand-sm w3-green fixed-top" style={{width: 100+'%',height: 50+'px'}}>
        <div>
            <i className='fas fa-arrow-left' onClick={back} style={{size: 20+'px',fontSize: 20+'px'}}>စီစင်သူ</i>
        </div>            
    </nav>
        <div className="map" style={{width:100+'%',position:'absolute',top:70,left:20}}>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59214.587194538835!2d96.06055907789472!3d21.937945785513!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30cb6da2abd3b643%3A0x5e8365a2067901fb!2sPico%20Innovation!5e0!3m2!1sen!2smm!4v1586320080923!5m2!1sen!2smm"
             frameborder="0" style={{width:100+'%',height:400,border:0,allowfullscreen:'',tabindex:0}} ></iframe>
        </div>
        <div className="footer">
            <table id="changeTable2">
            <tfoot>
            <tr>
            <td><Link to="./ahluShin"><i className="fas fa-donate" style={{color:'black'}}></i><br/><p>အလှူရှင်</p></Link></td>
            <td><Link to="./ahlukhan"><i className="fa fa-medkit" style={{color:'black'}}></i><br/><p>အလှူခံ</p></Link></td>
            <td><Link to="./sayinmyar"><i className="fas fa-newspaper" style={{color:'black'}}></i><br/><p>စာရင်းများ</p></Link></td>
            <td><a href="#"><i className="fas fa-comment" style={{color:'black'}}></i><br/><p>Chat</p></a></td>
            </tr>
            </tfoot>
            
            </table>
            </div>
</div>
    );
}