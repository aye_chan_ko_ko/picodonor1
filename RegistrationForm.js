import React from 'react';
import './RegistrationForm.css';
import  {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";
export default function RegistrationForm(){
    let history = useHistory();
    console.log(history)
    let back = e => {
       e.stopPropagation();
       history.goBack();
       };
    return(
        <div className="container">
        <div>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <i className="fas fa-check" style={{color:'white'}}></i>
                <h5 class="modal-title" id="exampleModalLabel">New Item</h5>
                <button onClick={back} type="button" style={{color:'white'}} class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div className="modal-body">
                    <form style={{maxWidth:450,margin:'auto'}}> 
                        <div className="form-group"> 
                        <i className="far fa-hospital icon"></i>                
                        <input className="form-control" 
                                   type="text" 
                                   placeholder="Hospital Name"/> 
                        </div> 
                    
              
                        <div className="form-group"> 
                            <i className="far fa-address-book icon"></i> 
                            <input className="form-control" 
                                   type="text"
                                   placeholder="Contact Person"/> 
                        </div> 
                        <div className="form-group"> 
                            <i className="fas fa-bars icon"></i>
                            <input className="form-control" 
                                   type="text"
                                   placeholder="Required Items"/> 
                        </div> 
                        <div className="form-group"> 
                            <i className="fas fa-list-ol icon"></i>
                            <input className="form-control" 
                                   type="number"
                                   placeholder="Qty Needed"/> 
                        </div> 
                        <div className="form-group"> 
                            <i className="far fa-building icon"></i> 
                            <input class="form-control" 
                                   type="text"
                                   placeholder="City/Township"/> 
                        </div> 
                        <div className="form-group"> 
                            <i className="fas fa-phone icon"></i>
                            <input className="form-control" 
                                   type="number"
                                   placeholder="Phone"/> 
                        </div>
                        </form>
                        <div class="modal-footer">
                        <button type="button" class="btn">Add</button>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
                      
    );
}